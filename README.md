Ledonardo
=========

A light painting experiment
---------------------------

For details, see [my article about this project](https://www.stavros.io/posts/behold-ledonardo/).


Instructions
------------

This project has no instructions, mainly because I didn't have time to write any.
All you need to run it is a WeMos D1 mini with an SD shield and MicroPython.
The rest should be pretty easy to figure out yourself.


License
-------

The code and all the associated files are distributed under the GNU GPL 3.0, with the additional provision that, if you
use any of the code or files in this project, you must credit the original repo in a prominent place.
