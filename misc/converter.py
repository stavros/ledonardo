#!/usr/bin/env python3
"""Light Tracer

Usage:
    converter.py [options] <infile> <outfile>

Options:
    -h --help                 Show this screen.

    -l --rtl                  Draw the pixels left to right.

    -d --density=<density>    The LED strip's density, in LEDs per meter, so we
                              can calculate the horizontal resolution [default: 60]

    -g --height=<height>      The height of the strip in pixels [default: 121].
"""
import array
import struct
from itertools import chain
from pathlib import Path
from typing import List

from docopt import docopt
from PIL import Image


def correct_gamma(intensity, gamma=2.8):
    return int(((intensity / 255) ** gamma) * 255)


def image_to_list(image: Image, rtl: bool) -> List[array.array]:
    """
    Convert a PIL image to a list of arrays.

    The returned list has a length of image.width.

    rtl - Whether to draw the pixels left-to-right or right-to-left.
    """
    (width, height) = image.size
    image_data = image.getdata()
    image_list = []
    if rtl:
        order = [width - 1, -1, -1]
    else:
        order = [width]

    for col in range(*order):
        # We read from bottom to top here, as the strip also goes bottom-to-top.
        pixel_row = [
            image_data[(row * width) + col] for row in range(height - 1, -1, -1)
        ]

        pixel_row = [tuple(correct_gamma(i) for i in r) for r in pixel_row]

        # NeoPixels aren't RGB, they're GRB, so we perform that transposition here.
        neopixel_row = [(g, r, b) for (r, g, b) in pixel_row]

        image_list.append(array.array("B", chain(*neopixel_row)))
    return image_list


def resize_image(filename: str, height: int, rtl: bool, outfile: str) -> Image:
    """
    Resize an image according to the strip's needs.

    Since the LED strip has a fixed vertical resolution, but effectively
    infinite horizontal resolution, we resize the image in the vertical but
    choose a different resolution in the horizontal.
    """
    im = Image.open(filename).convert("RGB")

    old_width, old_height = im.size
    old_width = min(old_width, 500)

    # Resize the image.
    im = im.resize((old_width, height), Image.NEAREST)

    with open(outfile, "wb") as of:
        of.write(struct.pack("QQQ", old_width, old_height, height))
        for column in image_to_list(im, rtl):
            of.write(column.tobytes())


def main(arguments: dict) -> None:
    rtl = arguments["--rtl"]
    height = int(arguments["--height"])

    infile = Path(arguments["<infile>"])
    outfile = Path(arguments["<outfile>"])
    if infile.is_dir():
        for fn in infile.iterdir():
            if fn.suffix.lower() not in (".png", ".jpg"):
                continue
            print("Converting %s..." % fn)
            resize_image(
                fn,
                height=height,
                rtl=rtl,
                outfile=(outfile / fn.with_suffix(".lti").name),
            )
    else:
        resize_image(infile, height=height, rtl=rtl, outfile=outfile)


if __name__ == "__main__":
    arguments = docopt(__doc__)
    main(arguments)
