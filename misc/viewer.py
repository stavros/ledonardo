#!/usr/bin/env python

import array
import struct
import sys

import pygame


def get_pixel_triples(column):
    """
    Given an iterable of pixels ([x1, y1, z1, x2, y2, z2, x3, y3, z3]), return
    a list of [(x3, y3, z3), (x2, y2, z2), (x1, y1, z1)].

    The sequence does not need to be reversed because the data was already sent
    bottom-to-top.
    """
    for counter in range(0, len(column), 3):
        yield (column[counter:counter + 3])


def show_file(filename):
    pygame.init()
    with open(filename, "rb") as infile:
        width, old_height, pixels = struct.unpack_from("QQQ", infile.read(24))
        print(width, old_height, pixels)
        screen = pygame.display.set_mode((width, pixels))
        for col in range(width):
            data = infile.read(pixels * 3)
            column = array.array("B", data)
            for counter, rgb in enumerate(get_pixel_triples(column)):
                r, g, b = rgb
                # NeoPixels are GRB, so we compensate for that here.
                screen.set_at((col, pixels - counter), pygame.Color(g, r, b))
    pygame.display.flip()
    print("Press any key to continue...")
    raw_input()


def main():
    show_file(sys.argv[1])


if __name__ == "__main__":
    main()
