# This file is executed on every boot (including wake-boot from deepsleep)
import gc

import esp
import machine
from flashbdev import bdev

gc.collect()


def set_adc_mode(vcc_mode=True):
    """Set the ADC mode to voltage or analog."""
    mode = 255 if vcc_mode else 0
    sector_size = bdev.SEC_SIZE
    flash_size = esp.flash_size()  # device dependent
    init_sector = int(flash_size / sector_size - 4)
    data = bytearray(esp.flash_read(init_sector * sector_size, sector_size))
    if data[107] == mode:
        return  # flash is already correct; nothing to do
    else:
        data[107] = mode  # re-write flash
        esp.flash_erase(init_sector)
        esp.flash_write(init_sector * sector_size, data)
        print("ADC mode changed in flash; restart to use it!")
        return


def mount_sd():
    import machine, sdcard, os

    machine.freq(160000000)

    spi = machine.SPI(1)
    sd = sdcard.SDCard(spi, machine.Pin(15))
    os.umount("/")
    os.mount(sd, "/")
    os.chdir("/")


set_adc_mode()

try:
    print("Mounting SD card...")
    mount_sd()
except:
    print("Could not mount SD card.")
else:
    import pytracer

    try:
        pytracer.main()
    except OSError:
        machine.reset()
