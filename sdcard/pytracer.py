import array
import gc
import os
import sys

try:
    import network
    import machine
    import ustruct as struct
    import usocket as socket
    import neopixel
    from utime import sleep_ms, ticks_ms

    LED_PIN = machine.Pin(2, machine.Pin.OUT)
    LED_PIN.on()
except ImportError:
    import pygame
    import struct
    import socket

    machine = None
    pcounter = 1

    pygame.init()
    screen = pygame.display.set_mode((900, 200))

    ticks_ms = lambda: 0

    def sleep_ms(s):
        from time import sleep

        sleep(s / 1000.0)

    class LED_PIN:
        def on():
            pass

        off = on


IMAGE_DIR = "/lighttracer/images"


# This is a object we use to defer execution of the image starting function and hold its parameters until
# after we return the response to the browser.
DEFERRED_EXEC = None


###############
# HTTP Server
#

HTTP_OK = b"200 OK"
HTTP_NOT_FOUND = b"404 Not Found"
HTTP_UNPROCESSABLE_ENTITY = b"422 Unprocessable Entity"


def home_view(method, params):
    """The homepage view handler."""
    page = open("/ui.min.html", "rb").read()

    vcc = machine.ADC(1)
    voltage = vcc.read()

    filenames = [
        b'<option value="%s">%s</option>' % (filename, filename)
        for filename in sorted(os.listdir(IMAGE_DIR))
        if filename.endswith(".lti")
    ]

    return HTTP_OK, page % (voltage / 1000, b"".join(filenames))


def execute_view(method, params):
    """The execution view handler."""
    if len(params) != 4:
        return HTTP_UNPROCESSABLE_ENTITY, b"Could not execute, no parameters sent."

    name = params.get(b"image", b"").decode("utf8")
    delay = float(params.get(b"delay", b"").decode("utf8"))
    speed = float(params.get(b"duration", b"").decode("utf8")) / 1000
    dim = float(params.get(b"brightness", b"").decode("utf8")) / 100
    print("Starting:", name, delay, speed, dim)

    global DEFERRED_EXEC
    DEFERRED_EXEC = {"name": name, "delay": delay, "speed": speed, "dim": dim}

    return HTTP_OK, b"Starting display of %s..." % (name,)


def handle_request(request):
    """Parse and route the incoming HTTP request."""
    first = request[: request.find(b"\n")]
    method, path, _ = first.split(b" ")
    path = path.rstrip(b"/?")

    if b"?" in path:
        path, params = path.split(b"?")
        params = dict(pair.split(b"=") for pair in params.split(b"&"))
    else:
        params = {}

    return {b"": home_view, b"/execute": execute_view}.get(
        path, lambda x, y: (HTTP_NOT_FOUND, b"Not found.")
    )(method, params)


def serve(port=80):
    """Start the HTTP server."""
    led = machine.Pin(2, machine.Pin.OUT)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(("", port))
    s.listen(2)
    s.settimeout(1.0)
    last_blink = -100000  # We want the LED to start blinking right away on boot.
    while True:
        print("Waiting for connection...")
        try:
            conn, addr = s.accept()
        except OSError:
            # Connection timed out, reloop.
            if ticks_ms() - last_blink > 65000:
                # Wait before blinking the LED, so we don't get it in the shot.
                led.off()
                sleep_ms(10)
                led.on()
            continue

        print("Connection came in.")
        last_blink = ticks_ms()
        request = conn.recv(1024)
        try:
            code, response = handle_request(request)

            conn.sendall(b"HTTP/1.1 %s\n" % code)
            conn.sendall(
                b"Connection: close\nServer: LightTracer\nContent-Type: text/html\n\n"
            )
            conn.sendall(response)
            conn.sendall("\n")
        except Exception as e:
            print("Request error.")
            sys.print_exception(e)

            del code
            del response
        conn.close()

        del conn
        del request
        gc.collect()

        global DEFERRED_EXEC
        if DEFERRED_EXEC:
            # Handle deferred execution after everything has been returned and the connection has been closed.
            start_image(
                DEFERRED_EXEC["name"],
                DEFERRED_EXEC["delay"],
                DEFERRED_EXEC["speed"],
                DEFERRED_EXEC["dim"],
            )
            DEFERRED_EXEC = None


###############
# LightTracer
#


def get_pixel_triples(column):
    """
    Given an iterable of pixels ([x1, y1, z1, x2, y2, z2, x3, y3, z3]), return
    a list of [(x3, y3, z3), (x2, y2, z2), (x1, y1, z1)].

    The sequence does not need to be reversed because the data was already sent
    bottom-to-top.
    """
    for counter in range(0, len(column), 3):
        yield (column[counter : counter + 3])


class ImagePrinter:
    def __init__(self, pixel_duration, initial_pause, num_pixels=90, darken=1):
        """
        Initialize the image printer.

        pixel_duration   - The duration of each pixel, in seconds.
        initial_pause    - The number of seconds to pause before starting.
        """
        self.pixel_duration = int(pixel_duration * 1000)
        self._initial_pause = int(initial_pause * 1000)
        self._num_pixels = int(num_pixels)
        self._darken = darken

        if machine:
            self._np = neopixel.NeoPixel(machine.Pin(4), self._num_pixels)
        else:
            self._np = None

        self._clear_strip()

    def _clear_strip(self):
        """
        Clear the LED strip.
        """
        print("Clearing the strip...")
        if not machine:
            return

        self._np.buf = b"\x00" * (self._num_pixels * 3)
        self._np.write()

    def show_pygame_column_and_pause(self, column):
        global pcounter
        collen = int(len(list(column)) / 3)
        for counter, rgb in enumerate(get_pixel_triples(column)):
            rgb = [int(x * self._darken) for x in rgb]
            screen.set_at((pcounter, collen - counter), pygame.Color(*rgb))
        pcounter += 1
        pygame.display.flip()

    def show_column_and_pause(self, column):
        """
        Show a single column for a distance and pause.

        column should be an array object.
        """
        start_ms = ticks_ms()

        if 0 < self._darken < 1:
            self._np.buf = bytearray(int(x * self._darken) for x in column)
        else:
            self._np.buf = column
        self._np.write()

        sleep_ms(self.pixel_duration - (ticks_ms() - start_ms) - 9)

    def initial_pause(self):
        """
        Perform the initial pause.
        """
        sleep_ms(self._initial_pause)


def start_image(filename, initial_pause, pixel_duration, darken):
    with open("%s/%s" % (IMAGE_DIR, filename), "rb") as infile:
        # Read metadata from file.
        width, old_height, pixels = struct.unpack_from("QQQ", infile.read(24))
        print("Starting image with %s, %s, %s..." % (width, old_height, pixels))

        ip = ImagePrinter(pixel_duration, initial_pause, pixels, darken)
        ip.initial_pause()

        if not machine:
            for _ in range(width):
                data = infile.read(pixels * 3)
                ip.show_pygame_column_and_pause(array.array("B", data))
        else:
            for _ in range(width):
                data = infile.read(pixels * 3)
                ip.show_column_and_pause(data)

        ip._clear_strip()
        print("Image done with %d columns." % width)


def serve_udp(port=1234):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    s.bind(("", port))

    while True:
        # Wait a bit.
        sleep_ms(1)

        # The first line should be something like "batman.lti|1|0.1|0.6".
        data, addr = s.recvfrom(2048)

        try:
            filename, initial_pause, pixel_duration, darken = (
                data.decode("utf8").strip().split("|")
            )
        except:  # noqa
            continue

        initial_pause = float(initial_pause)
        pixel_duration = float(pixel_duration)
        darken = float(darken)

        print(
            "Parameters are: %s, %s, %s, %s"
            % (filename, initial_pause, pixel_duration, darken)
        )

        start_image(filename, initial_pause, pixel_duration, darken)


def main():
    print("Starting up WiFi AP...")
    if machine:
        ap = network.WLAN(network.AP_IF)
        ap.active(True)
        ap.config(essid="Ledonardo", authmode=3, password="some password")

    # Clear the strip.
    ImagePrinter(1, 0, 200, 1)
    print("Starting up LightTracer server...")
    serve(80)


if __name__ == "__main__":
    main()
