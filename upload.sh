#!/bin/bash

echo "Minifying..."
mpy-cross sdcard/pytracer.py
minify sdcard/ui.html > sdcard/ui.min.html

echo "Uploading..."
ampy -p /dev/ttyUSB0 --baud 115200 put sdcard/pytracer.mpy
ampy -p /dev/ttyUSB0 --baud 115200 put sdcard/ui.min.html

echo "Cleaning up..."
rm sdcard/pytracer.mpy
rm sdcard/ui.min.html
